package com.vegaitproject.VegaITproject.models;

import com.vegaitproject.VegaITproject.enums.MemberStatus;
import com.vegaitproject.VegaITproject.enums.Role;
import lombok.Data;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

@Data
@Entity
public class TeamMember {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;


    private String name;

    private String username;

    private String email;

    private float dailyHours;

    private MemberStatus status;

    private Role role;

    private boolean isDeleted;
}
