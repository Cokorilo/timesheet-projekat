package com.vegaitproject.VegaITproject.models;

import lombok.Data;
import javax.persistence.*;
import java.time.LocalDate;

@Data
@Entity
public class Activity {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Client client;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Project project;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Category category;

    @ManyToOne(cascade = CascadeType.MERGE)
    private TeamMember teamMember;

    private String description;

    private float time;

    private float overtime;

    private LocalDate date;

    private boolean isDeleted;
}
