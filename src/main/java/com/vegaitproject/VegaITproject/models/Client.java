package com.vegaitproject.VegaITproject.models;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class Client {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    private String address;

    private String city;

    private long postalCode;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Country country;

    private boolean isDeleted;
}
