package com.vegaitproject.VegaITproject.models;

import com.vegaitproject.VegaITproject.enums.ProjectStatus;
import lombok.Data;
import javax.persistence.*;

@Data
@Entity
public class Project {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    private String description;

    @ManyToOne(cascade = CascadeType.MERGE)
    private Client customer;

    @ManyToOne(cascade = CascadeType.MERGE)
    private TeamMember lead;

    private ProjectStatus status;

    private boolean archive;

    private boolean isDeleted;
}
