package com.vegaitproject.VegaITproject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class VegaItProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(VegaItProjectApplication.class, args);
	}

}
