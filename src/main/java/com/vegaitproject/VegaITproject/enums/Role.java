package com.vegaitproject.VegaITproject.enums;

public enum Role {
    ADMIN,
    WORKER
}
