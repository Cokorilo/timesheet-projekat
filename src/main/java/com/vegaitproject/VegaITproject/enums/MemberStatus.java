package com.vegaitproject.VegaITproject.enums;

public enum MemberStatus {
    ACTIVE,
    INACTIVE
}
