package com.vegaitproject.VegaITproject.enums;

public enum ProjectStatus {
    ACTIVE,
    INACTIVE
}
