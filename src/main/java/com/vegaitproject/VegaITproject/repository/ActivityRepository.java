package com.vegaitproject.VegaITproject.repository;

import com.vegaitproject.VegaITproject.models.Activity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.time.LocalDate;
import java.util.List;

@Repository
public interface ActivityRepository extends JpaRepository<Activity,Integer> {


    @Query("select a from Activity a where a.isDeleted = :isDeleted")
    List<Activity> findAllByDeleted(@Param("isDeleted") Boolean isDeleted);

    @Query(value = "select a from Activity a " +
            "where (:teamMemberId is null or a.teamMember.id = :teamMemberId)" +
            "and (:clientId is null or a.client.id = :clientId)" +
            "and (:projectId is null or a.project.id = :projectId )" +
            "and (:categoryId is null or a.category.id = :categoryId )" +
            "and (:startDate is null or a.date >= :startDate)" +
            "and (:endDate is null or a.date < :endDate)" +
            "and a.isDeleted = false")
    Page<Activity> findActivityInReports(@Param("teamMemberId") Integer teamMemberId,
                                         @Param("clientId") Integer clientId,
                                         @Param("projectId") Integer projectId,
                                         @Param("categoryId") Integer categoryId,
                                         @Param("startDate") LocalDate startDate, @Param("endDate") LocalDate endDate,
                                         Pageable pageable);

    List<Activity> findAllByDateAndTeamMemberIdAndIsDeleted(@Param("date") LocalDate date, @Param("teamMemberId") Integer teamMemberId, @Param("isDeleted") Boolean isDeleted);
}
