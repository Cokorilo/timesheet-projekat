package com.vegaitproject.VegaITproject.repository;

import com.vegaitproject.VegaITproject.models.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import java.util.List;

@Repository
public interface CategoryRepository extends JpaRepository<Category,Integer> {

    @Query("select c from Category c where c.isDeleted = :isDeleted")
    List<Category> findAllByDeleted(@Param("isDeleted") Boolean isDeleted);
}
