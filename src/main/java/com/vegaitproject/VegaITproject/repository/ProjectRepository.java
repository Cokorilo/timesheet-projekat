package com.vegaitproject.VegaITproject.repository;

import com.vegaitproject.VegaITproject.models.Project;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectRepository extends PagingAndSortingRepository<Project,Integer> {

    @Query("select p from Project p where p.isDeleted = :isDeleted")
    Page<Project> findAllByDeleted(@Param("isDeleted") Boolean isDeleted, Pageable pageable);

    @Query("select p from Project p where p.name like :keyword and p.isDeleted = false")
    Page<Project> findByNameLike(@Param("keyword") String keyword, Pageable pageable);
}
