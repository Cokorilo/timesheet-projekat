package com.vegaitproject.VegaITproject.repository;

import com.vegaitproject.VegaITproject.models.Client;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientRepository extends PagingAndSortingRepository<Client,Integer> {

    @Query("select c from Client c where c.isDeleted = :isDeleted")
    Page<Client> findAllByDeleted(@Param("isDeleted") Boolean isDeleted, Pageable pageable);

    @Query("select c from Client c where c.name like :keyword and c.isDeleted = false")
    Page<Client> findByNameLike(@Param("keyword") String keyword, Pageable pageable);
}
