package com.vegaitproject.VegaITproject.repository;

import com.vegaitproject.VegaITproject.models.TeamMember;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface TeamMemberRepository extends PagingAndSortingRepository<TeamMember,Integer> {

    @Query("select tm from TeamMember tm where tm.isDeleted = :isDeleted")
    Page<TeamMember> findAllByDeleted(@Param("isDeleted") Boolean isDeleted, Pageable pageable);
}
