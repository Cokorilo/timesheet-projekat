package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.TeamMemberDTO;
import com.vegaitproject.VegaITproject.error.EntityNotFoundException;
import com.vegaitproject.VegaITproject.models.TeamMember;
import com.vegaitproject.VegaITproject.repository.TeamMemberRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class TeamMemberServiceImpl implements TeamMemberService{

    @Autowired
    private TeamMemberRepository teamMemberRepository;

    @Override
    public TeamMemberDTO saveTeamMember(TeamMemberDTO teamMemberDTO) {
        TeamMember teamMember = this.convertDtoToEntity(teamMemberDTO);
        TeamMember savedTeamMember = teamMemberRepository.save(teamMember);
        return convertEntityToDto(savedTeamMember);
    }

    @Override
    public Page<TeamMemberDTO> getAllTeamMembers(int page, int size) {
        Pageable firstPageWithFiveElements = PageRequest.of(page, size);
        Page<TeamMember> teamMembers = teamMemberRepository.findAllByDeleted(false, firstPageWithFiveElements);
        List<TeamMemberDTO> teamMemberDTOList = new ArrayList<>();
        for (TeamMember teamMember: teamMembers){
            teamMemberDTOList.add(convertEntityToDto(teamMember));
        }
        return new PageImpl<>(teamMemberDTOList, teamMembers.getPageable(), teamMembers.getTotalElements());
    }

    @Override
    public TeamMemberDTO getTeamMemberById(int id) {
        return convertEntityToDto(teamMemberRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Team Member not found for id: " + id)));
    }

    @Override
    public void deleteTeamMemberById(int id) {
        TeamMember deleteTeamMember = teamMemberRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Team Member not found for id: " + id));
        deleteTeamMember.setDeleted(true);
        teamMemberRepository.save(deleteTeamMember);
    }

    @Override
    public void deleteAllTeamMembers()  {
        List<TeamMember> teamMembers = (List<TeamMember>) teamMemberRepository.findAll();
        for(TeamMember teamMember: teamMembers){
            teamMember.setDeleted(true);
            teamMemberRepository.save(teamMember);
        }
    }

    @Override
    public TeamMemberDTO updateTeamMember(TeamMemberDTO teamMemberDTO, int id) {
        TeamMember updateTeamMember = teamMemberRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Team Member not found for id: " + id));

        updateTeamMember.setName(teamMemberDTO.getName());
        updateTeamMember.setUsername(teamMemberDTO.getUsername());
        updateTeamMember.setEmail(teamMemberDTO.getEmail());
        updateTeamMember.setDailyHours(teamMemberDTO.getDailyHours());
        updateTeamMember.setStatus(teamMemberDTO.getStatus());
        updateTeamMember.setRole(teamMemberDTO.getRole());
        TeamMember savedTeamMember = teamMemberRepository.save(updateTeamMember);

        return convertEntityToDto(savedTeamMember);
    }
    @Override
    public TeamMemberDTO convertEntityToDto(TeamMember teamMember){
        TeamMemberDTO teamMemberDTO = new TeamMemberDTO();

        teamMemberDTO.setId(teamMember.getId());
        teamMemberDTO.setName(teamMember.getName());
        teamMemberDTO.setUsername(teamMember.getUsername());
        teamMemberDTO.setEmail(teamMember.getEmail());
        teamMemberDTO.setDailyHours(teamMember.getDailyHours());
        teamMemberDTO.setStatus(teamMember.getStatus());
        teamMemberDTO.setRole(teamMember.getRole());

        return teamMemberDTO;
    }

    @Override
    public TeamMember convertDtoToEntity(TeamMemberDTO teamMemberDTO) {
        TeamMember teamMember = new TeamMember();

        teamMember.setId(teamMemberDTO.getId());
        teamMember.setName(teamMemberDTO.getName());
        teamMember.setUsername(teamMemberDTO.getUsername());
        teamMember.setEmail(teamMemberDTO.getEmail());
        teamMember.setDailyHours(teamMemberDTO.getDailyHours());
        teamMember.setStatus(teamMemberDTO.getStatus());
        teamMember.setRole(teamMemberDTO.getRole());

        return teamMember;
    }
}
