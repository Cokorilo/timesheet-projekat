package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.ClientDTO;
import com.vegaitproject.VegaITproject.error.EntityNotFoundException;
import com.vegaitproject.VegaITproject.models.Client;
import com.vegaitproject.VegaITproject.repository.ClientRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class ClientServiceImpl implements ClientService{

    private static final String PERCENT_SIGN = "%";
    @Autowired
    ClientRepository clientRepository;

    @Autowired
    CountryService countryService;

    @Override
    public ClientDTO saveClient(ClientDTO clientDTO) {
        Client client = this.convertDtoToEntity(clientDTO);
        Client savedClient= clientRepository.save(client);
        return convertEntityToDto(savedClient);
    }

    @Override
    public Page<ClientDTO> getAllClients(int page, int size) {
        Pageable firstPageWithFiveElements = PageRequest.of(page, size);
        Page<Client> clients = clientRepository.findAllByDeleted(false, firstPageWithFiveElements);
        List<ClientDTO> clientDTOList = new ArrayList<>();
        for (Client client: clients){
            clientDTOList.add(convertEntityToDto(client));
        }
        return new PageImpl<>(clientDTOList, clients.getPageable(), clients.getTotalElements());
    }

    @Override
    public ClientDTO getClientById(int id) {
        return convertEntityToDto(clientRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Client not found for id: " + id)));
    }

    @Override
    public void deleteClientById(int id) {
        Client deleteClient = clientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Client not found for id: " + id));
        deleteClient.setDeleted(true);
        clientRepository.save(deleteClient);
    }

    @Override
    public void deleteAllClients() {
        List<Client> clients = (List<Client>) clientRepository.findAll();
        for(Client client: clients){
            client.setDeleted(true);
            clientRepository.save(client);
        }
    }

    @Override
    public Page<ClientDTO> getByNameLike(int page, int size, String keyword, boolean isLetter) {

        Pageable firstPageWithFiveElements = PageRequest.of(page, size);
        keyword = keyword.concat(PERCENT_SIGN);

        if(!isLetter) {
            keyword = PERCENT_SIGN.concat(keyword).concat(PERCENT_SIGN);
        }
        Page<Client> clients = clientRepository.findByNameLike(keyword, firstPageWithFiveElements);
        List<ClientDTO> clientDTOList = new ArrayList<>();
        for(Client client: clients){
            clientDTOList.add(convertEntityToDto(client));
        }
        return new PageImpl<>(clientDTOList, clients.getPageable(), clients.getTotalElements());
    }
    @Override
    public ClientDTO updateClient(ClientDTO clientDTO, int id) {
        Client updateClient = clientRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Client not found for id: " + id));

        updateClient.setName(clientDTO.getName());
        updateClient.setAddress(clientDTO.getAddress());
        updateClient.setCity(clientDTO.getCity());
        updateClient.setPostalCode(clientDTO.getPostalCode());
        updateClient.setCountry(countryService.convertDtoToEntity(countryService.getCountryById(clientDTO.getCountryId())));
        Client savedClient = clientRepository.save(updateClient);

        return convertEntityToDto(savedClient);
    }

    @Override
    public ClientDTO convertEntityToDto(Client client) {
        ClientDTO clientDTO = new ClientDTO();

        clientDTO.setId(client.getId());
        clientDTO.setName(client.getName());
        clientDTO.setAddress(client.getAddress());
        clientDTO.setCity(client.getCity());
        clientDTO.setPostalCode(client.getPostalCode());
        clientDTO.setCountryId(countryService.convertDtoToEntity(countryService.getCountryById(client.getCountry().getId())).getId());

        return clientDTO;
    }

    @Override
    public Client convertDtoToEntity(ClientDTO clientDTO) {
        Client client = new Client();

        client.setId(clientDTO.getId());
        client.setName(clientDTO.getName());
        client.setAddress(clientDTO.getAddress());
        client.setCity(clientDTO.getCity());
        client.setPostalCode(clientDTO.getPostalCode());
        client.setCountry(countryService.convertDtoToEntity(countryService.getCountryById(clientDTO.getCountryId())));

        return client;
    }
}
