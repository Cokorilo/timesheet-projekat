package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.ProjectDTO;
import com.vegaitproject.VegaITproject.models.Project;
import org.springframework.data.domain.Page;

public interface ProjectService {
    public ProjectDTO saveProject(ProjectDTO projectDTO);

    public Page<ProjectDTO> getAllProjects(int page, int size);

    public ProjectDTO getProjectById(int id);

    public void deleteProjectById(int id);

    public void deleteAllProjects();

    public Page<ProjectDTO> getByNameLike(int page, int size, String keyword, boolean isLetter);
    public ProjectDTO updateProject(ProjectDTO projectDTO, int id);

    public ProjectDTO convertEntityToDto(Project project);

    public Project convertDtoToEntity(ProjectDTO projectDTO);
}
