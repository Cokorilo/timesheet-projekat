package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.ActivityDTO;
import com.vegaitproject.VegaITproject.dto.CalendarDTO;
import com.vegaitproject.VegaITproject.dto.DayInMonthActivityDTO;
import com.vegaitproject.VegaITproject.dto.ReportDTO;
import com.vegaitproject.VegaITproject.models.Activity;
import org.springframework.data.domain.Page;
import java.time.LocalDate;
import java.util.List;

public interface ActivityService {

    public ActivityDTO saveActivity(ActivityDTO activityDTO);

    public List<ActivityDTO> getAllActivities();

    public ActivityDTO getActivityById(int id);

    public Page<ActivityDTO> getActivityInReports(ReportDTO reportDTO, int page, int size);
    public void deleteActivityById(int id);

    public void deleteAllActivities();

    public List<DayInMonthActivityDTO> returnCalendar(CalendarDTO calendarDTO);

    public List<ActivityDTO> returnActivityByDate(LocalDate localDate, int id);

    public ActivityDTO updateActivity(ActivityDTO activityDTO, int id);

    public ActivityDTO convertEntityToDto(Activity activity);

    public Activity convertDtoToEntity(ActivityDTO activityDTO);
}
