package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.TeamMemberDTO;
import com.vegaitproject.VegaITproject.models.TeamMember;
import org.springframework.data.domain.Page;

public interface TeamMemberService {

    public TeamMemberDTO saveTeamMember(TeamMemberDTO teamMember);

    public Page<TeamMemberDTO> getAllTeamMembers(int page ,int size);

    public TeamMemberDTO getTeamMemberById(int id);

    public void deleteTeamMemberById(int id);

    public void deleteAllTeamMembers();

    public TeamMemberDTO updateTeamMember(TeamMemberDTO teamMember, int id);

    public TeamMemberDTO convertEntityToDto(TeamMember teamMember);

    public TeamMember convertDtoToEntity(TeamMemberDTO teamMemberDTO);
}
