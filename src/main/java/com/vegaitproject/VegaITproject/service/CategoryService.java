package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.CategoryDTO;
import com.vegaitproject.VegaITproject.models.Category;
import java.util.List;

public interface CategoryService {
    public CategoryDTO saveCategory(CategoryDTO category);
    public List<CategoryDTO> getAllCategories();

    public CategoryDTO getCategoryById(int id);

    public void deleteCategoryById(int id);

    public void deleteAllCategories();

    public CategoryDTO updateCategory(CategoryDTO category, int id);


    public CategoryDTO convertEntityToDto(Category category);

    public Category convertDtoToEntity(CategoryDTO categoryDTO);
}
