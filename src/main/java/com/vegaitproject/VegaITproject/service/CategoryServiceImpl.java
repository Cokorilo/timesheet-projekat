package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.CategoryDTO;
import com.vegaitproject.VegaITproject.error.EntityNotFoundException;
import com.vegaitproject.VegaITproject.models.Category;
import com.vegaitproject.VegaITproject.repository.CategoryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service()
public class CategoryServiceImpl implements CategoryService {

    @Autowired
    private CategoryRepository categoryRepository;
    @Override
    public CategoryDTO saveCategory(CategoryDTO categoryDTO) {
        Category category = this.convertDtoToEntity(categoryDTO);
        Category savedCategory = categoryRepository.save(category);
        return convertEntityToDto(savedCategory);
    }

    @Override
    public List<CategoryDTO> getAllCategories() {
        List<Category> categories = categoryRepository.findAllByDeleted(false);
        List<CategoryDTO> categoryDTOList = new ArrayList<>();
        for (Category category: categories){
            categoryDTOList.add(convertEntityToDto(category));
        }
        return categoryDTOList;
    }

    @Override
    public CategoryDTO getCategoryById(int id) {
        return convertEntityToDto(categoryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Category not found for id: " + id)));
    }

    @Override
    public void deleteCategoryById(int id) {
        Category deleteCategory = categoryRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Category not found for id: " + id));
        deleteCategory.setDeleted(true);
        categoryRepository.save(deleteCategory);
    }

    @Override
    public void deleteAllCategories() {
        List<Category> categories = categoryRepository.findAll();
        for(Category category: categories){
            category.setDeleted(true);
            categoryRepository.save(category);
        }
    }

    @Override
    public CategoryDTO updateCategory(CategoryDTO categoryDTO, int id) {
        Category updateCategory = categoryRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Category not found for id: " + id));
        updateCategory.setName(categoryDTO.getName());
        Category savedCategory = categoryRepository.save(updateCategory);
        return convertEntityToDto(savedCategory);
    }
    @Override
    public CategoryDTO convertEntityToDto(Category category){
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(category.getId());
        categoryDTO.setName(category.getName());
        return categoryDTO;
    }

    @Override
    public Category convertDtoToEntity(CategoryDTO categoryDTO) {
        Category category = new Category();
        category.setId(categoryDTO.getId());
        category.setName(categoryDTO.getName());
        return  category;
    }
}
