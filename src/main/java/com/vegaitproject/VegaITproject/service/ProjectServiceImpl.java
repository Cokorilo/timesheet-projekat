package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.ProjectDTO;
import com.vegaitproject.VegaITproject.error.EntityNotFoundException;
import com.vegaitproject.VegaITproject.models.Project;
import com.vegaitproject.VegaITproject.repository.ProjectRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service
public class ProjectServiceImpl implements ProjectService{

    private static final String PERCENT_SIGN = "%";

    @Autowired
    ProjectRepository projectRepository;

    @Autowired
    ClientService clientService;
    @Autowired
    TeamMemberService teamMemberService;

    @Override
    public ProjectDTO saveProject(ProjectDTO projectDTO) {
        Project project = this.convertDtoToEntity(projectDTO);
        Project savedProject = projectRepository.save(project);
        return convertEntityToDto(savedProject);
    }

    @Override
    public Page<ProjectDTO> getAllProjects(int page, int size) {
        Pageable firstPageWithFiveElements = PageRequest.of(page, size);
        Page<Project> projects = projectRepository.findAllByDeleted(false, firstPageWithFiveElements);
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for (Project project: projects){
            projectDTOList.add(convertEntityToDto(project));
        }
        return new PageImpl<>(projectDTOList, projects.getPageable(), projects.getTotalElements());
    }

    @Override
    public ProjectDTO getProjectById(int id) {
        return convertEntityToDto(projectRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Project not found for id: " + id)));
    }

    @Override
    public void deleteProjectById(int id) {
        Project deleteProject = projectRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Project not found for id: " + id));
        deleteProject.setDeleted(true);
        projectRepository.save(deleteProject);
    }

    @Override
    public void deleteAllProjects() {
        List<Project> projects = (List<Project>) projectRepository.findAll();
        for(Project project: projects){
            project.setDeleted(true);
            projectRepository.save(project);
        }
    }

    @Override
    public Page<ProjectDTO> getByNameLike(int page ,int size, String keyword, boolean isLetter) {

        Pageable firstPageWithFiveElements = PageRequest.of(page, size);
        keyword = keyword.concat(PERCENT_SIGN);

        if(!isLetter) {
            keyword = PERCENT_SIGN.concat(keyword).concat(PERCENT_SIGN);
        }

        Page<Project> projects = projectRepository.findByNameLike(keyword, firstPageWithFiveElements);
        List<ProjectDTO> projectDTOList = new ArrayList<>();
        for(Project project: projects){
            projectDTOList.add(convertEntityToDto(project));
        }
        return new PageImpl<>(projectDTOList, projects.getPageable(), projects.getTotalElements());
    }

    @Override
    public ProjectDTO updateProject(ProjectDTO projectDTO, int id) {
        Project updateProject = projectRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Project not found for id: " + id));

        updateProject.setName(projectDTO.getName());
        updateProject.setCustomer(clientService.convertDtoToEntity(clientService.getClientById(projectDTO.getCustomerId())));
        updateProject.setLead(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(projectDTO.getLeadId())));
        updateProject.setStatus(projectDTO.getStatus());
        updateProject.setArchive(projectDTO.isArchive());
        Project savedProject = projectRepository.save(updateProject);

        return convertEntityToDto(savedProject);
    }

    @Override
    public ProjectDTO convertEntityToDto(Project project) {
        ProjectDTO projectDTO = new ProjectDTO();

        projectDTO.setId(project.getId());
        projectDTO.setName(project.getName());
        projectDTO.setCustomerId(clientService.convertDtoToEntity(clientService.getClientById(project.getCustomer().getId())).getId());
        projectDTO.setLeadId(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(project.getLead().getId())).getId());
        projectDTO.setStatus(project.getStatus());
        projectDTO.setArchive(project.isArchive());

        return projectDTO;
    }

    @Override
    public Project convertDtoToEntity(ProjectDTO projectDTO) {
        Project project = new Project();

        project.setId(projectDTO.getId());
        project.setName(projectDTO.getName());
        project.setCustomer(clientService.convertDtoToEntity(clientService.getClientById(projectDTO.getCustomerId())));
        project.setLead(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(projectDTO.getLeadId())));
        project.setStatus(projectDTO.getStatus());
        project.setArchive(projectDTO.isArchive());

        return project;
    }
}
