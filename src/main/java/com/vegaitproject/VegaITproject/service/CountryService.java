package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.CountryDTO;
import com.vegaitproject.VegaITproject.models.Country;
import java.util.List;

public interface CountryService {

    public CountryDTO saveCountry(CountryDTO country);
    public List<CountryDTO> getAllCountries();

    public CountryDTO getCountryById(int id);

    public void deleteCountryById(int id);

    public void deleteAllCountries();

    public CountryDTO updateCountry(CountryDTO country, int id);

    public CountryDTO convertEntityToDto(Country country);

    public Country convertDtoToEntity(CountryDTO countryDTO);
}
