package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.ActivityDTO;
import com.vegaitproject.VegaITproject.dto.CalendarDTO;
import com.vegaitproject.VegaITproject.dto.DayInMonthActivityDTO;
import com.vegaitproject.VegaITproject.dto.ReportDTO;
import com.vegaitproject.VegaITproject.error.EntityNotFoundException;
import com.vegaitproject.VegaITproject.models.Activity;
import com.vegaitproject.VegaITproject.repository.ActivityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.*;
import org.springframework.stereotype.Service;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.YearMonth;
import java.time.temporal.TemporalAdjusters;
import java.util.ArrayList;
import java.util.List;

@Service
public class ActivityServiceImpl implements ActivityService{

    @Autowired
    ActivityRepository activityRepository;

    @Autowired
    ClientService clientService;

    @Autowired
    CategoryService categoryService;

    @Autowired
    ProjectService projectService;

    @Autowired
    TeamMemberService teamMemberService;

    @Override
    public ActivityDTO saveActivity(ActivityDTO activityDTO) {
        Activity activity = this.convertDtoToEntity(activityDTO);
        Activity savedActivity = activityRepository.save(activity);
        return convertEntityToDto(savedActivity);
    }

    @Override
    public List<ActivityDTO> getAllActivities() {
        List<Activity> activities = activityRepository.findAllByDeleted(false);
        List<ActivityDTO> activityDTOList = new ArrayList<>();
        for (Activity activity: activities){
            activityDTOList.add(convertEntityToDto(activity));
        }
        return activityDTOList;
    }

    @Override
    public ActivityDTO getActivityById(int id) {
        return convertEntityToDto(activityRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Activity not found for id: " + id)));
    }

    @Override
    public Page<ActivityDTO> getActivityInReports(ReportDTO reportDTO, int page, int size) {
        Pageable firstPageWithFiveElements = PageRequest.of(page, size, Sort.by(reportDTO.getSort()));
        Page<Activity> activities = activityRepository.findActivityInReports(reportDTO.getTeamMemberId(), reportDTO.getClientId(),
                reportDTO.getProjectId(), reportDTO.getCategoryId(), reportDTO.getStartDate(), reportDTO.getEndDate(), firstPageWithFiveElements);
        List<ActivityDTO> activityDTOList = new ArrayList<>();
        for (Activity activity: activities){
            activityDTOList.add(convertEntityToDto(activity));
        }
        return new PageImpl<>(activityDTOList, activities.getPageable(), activities.getTotalElements());
    }

    @Override
    public void deleteActivityById(int id) {
        Activity deleteActivity = activityRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Activity not found for id: " + id));
        deleteActivity.setDeleted(true);
        activityRepository.save(deleteActivity);
    }

    @Override
    public void deleteAllActivities() {
        List<Activity> activities = activityRepository.findAll();
        for(Activity activity: activities){
            activity.setDeleted(true);
            activityRepository.save(activity);
        }
    }

    @Override
    public List<DayInMonthActivityDTO> returnCalendar(CalendarDTO calendarDTO){
        YearMonth selectedMonth = YearMonth.of( calendarDTO.getYear(), calendarDTO.getMonth() );
        LocalDate firstOfMonth = selectedMonth.atDay(1).with(TemporalAdjusters.previousOrSame(DayOfWeek.MONDAY));
        LocalDate lastOfMonth = selectedMonth.atEndOfMonth().with(TemporalAdjusters.nextOrSame(DayOfWeek.SUNDAY));

        List<LocalDate> daysBetween = new ArrayList<>(firstOfMonth.datesUntil(lastOfMonth).toList());
        daysBetween.add(lastOfMonth);

        List<DayInMonthActivityDTO> daysToReturn = new ArrayList<>();
        for (LocalDate localDate: daysBetween) {
           DayInMonthActivityDTO dayInMonthActivityDTO = new DayInMonthActivityDTO();
           dayInMonthActivityDTO.setDate(localDate);
           dayInMonthActivityDTO.setDay(localDate.getDayOfWeek().toString());
           float hours = 0;
           List<Activity> activitiesForDate = activityRepository.findAllByDateAndTeamMemberIdAndIsDeleted(localDate, calendarDTO.getTeamMemberId(), false);
           for (Activity activity: activitiesForDate) {
               hours += activity.getTeamMember().getDailyHours();
           }
           dayInMonthActivityDTO.setHours(hours);
           String enabledOrDisabled = localDate.getMonth() != selectedMonth.getMonth() ? "disabled" : "enabled";
           dayInMonthActivityDTO.setEnabledOrDisabled(enabledOrDisabled);
           daysToReturn.add((dayInMonthActivityDTO));
        }

        return daysToReturn;
    }

    @Override
    public List<ActivityDTO> returnActivityByDate(LocalDate localDate, int id) {
        List<ActivityDTO> activities = new ArrayList<>();
        List<Activity> activitiesForDate = activityRepository.findAllByDateAndTeamMemberIdAndIsDeleted(localDate, id, false);
        for(Activity activity: activitiesForDate){
            activities.add(convertEntityToDto(activity));
        }
        return activities;
    }

    @Override
    public ActivityDTO updateActivity(ActivityDTO activityDTO, int id) {
        Activity updateActivity = activityRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Activity not found for id: " + id));

        updateActivity.setClient(clientService.convertDtoToEntity(clientService.getClientById(activityDTO.getClientId())));
        updateActivity.setProject(projectService.convertDtoToEntity(projectService.getProjectById(activityDTO.getProjectId())));
        updateActivity.setCategory(categoryService.convertDtoToEntity(categoryService.getCategoryById(activityDTO.getCategoryId())));
        updateActivity.setTeamMember(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(activityDTO.getTeamMemberId())));
        updateActivity.setDate(activityDTO.getDate());
        updateActivity.setTime(activityDTO.getTime());
        updateActivity.setDescription(activityDTO.getDescription());

        Activity savedActivity = activityRepository.save(updateActivity);
        return convertEntityToDto(savedActivity);
    }

    @Override
    public ActivityDTO convertEntityToDto(Activity activity) {
        ActivityDTO activityDTO = new ActivityDTO();

        activityDTO.setId(activity.getId());
        activityDTO.setClientId(clientService.convertDtoToEntity(clientService.getClientById(activity.getClient().getId())).getId());
        activityDTO.setClientName(clientService.convertDtoToEntity(clientService.getClientById(activity.getClient().getId())).getName());
        activityDTO.setProjectId(projectService.convertDtoToEntity(projectService.getProjectById(activity.getProject().getId())).getId());
        activityDTO.setProjectName(projectService.convertDtoToEntity(projectService.getProjectById(activity.getProject().getId())).getName());
        activityDTO.setCategoryId(categoryService.convertDtoToEntity(categoryService.getCategoryById(activity.getCategory().getId())).getId());
        activityDTO.setCategoryName(categoryService.convertDtoToEntity(categoryService.getCategoryById(activity.getCategory().getId())).getName());
        activityDTO.setTeamMemberId(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(activity.getTeamMember().getId())).getId());
        activityDTO.setTeamMemberName(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(activity.getTeamMember().getId())).getName());
        activityDTO.setTime(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(activity.getTeamMember().getId())).getDailyHours());
        activityDTO.setDate(activity.getDate());
        activityDTO.setDescription(activity.getDescription());

        return activityDTO;
    }

    @Override
    public Activity convertDtoToEntity(ActivityDTO activityDTO) {
        Activity activity = new Activity();

        activity.setId(activityDTO.getId());
        activity.setClient(clientService.convertDtoToEntity(clientService.getClientById(activityDTO.getClientId())));
        activity.setProject(projectService.convertDtoToEntity(projectService.getProjectById(activityDTO.getProjectId())));
        activity.setCategory(categoryService.convertDtoToEntity(categoryService.getCategoryById(activityDTO.getCategoryId())));
        activity.setTeamMember(teamMemberService.convertDtoToEntity(teamMemberService.getTeamMemberById(activityDTO.getTeamMemberId())));
        activity.setTime(activityDTO.getTime());
        activity.setDate(activityDTO.getDate());
        activity.setDescription(activityDTO.getDescription());

        return activity;
    }
}
