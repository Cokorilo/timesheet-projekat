package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.CountryDTO;
import com.vegaitproject.VegaITproject.error.EntityNotFoundException;
import com.vegaitproject.VegaITproject.models.Country;
import com.vegaitproject.VegaITproject.repository.CountryRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;

@Service()
public class CountryServiceImpl implements CountryService {

    @Autowired
    private CountryRepository countryRepository;
    @Override
    public CountryDTO saveCountry(CountryDTO countryDTO) {
        Country country = this.convertDtoToEntity(countryDTO);
        Country savedCountry = countryRepository.save(country);
        return convertEntityToDto(savedCountry);
    }

    @Override
    public List<CountryDTO> getAllCountries() {
        List<Country> countries = countryRepository.findAllByDeleted(false);
        List<CountryDTO> countryDTOList = new ArrayList<>();
        for (Country country: countries){
            countryDTOList.add(convertEntityToDto(country));
        }
        return countryDTOList;
    }

    @Override
    public CountryDTO getCountryById(int id) {
        return convertEntityToDto(countryRepository.findById(id)
                .orElseThrow(() -> new EntityNotFoundException("Country not found for id: " + id)));
    }
    @Override
    public void deleteCountryById(int id) {
        Country deletedCountry = countryRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Country not found for id: " + id));
        deletedCountry.setDeleted(true);
        countryRepository.save(deletedCountry);
    }

    @Override
    public void deleteAllCountries()  {
        List<Country> countries = countryRepository.findAll();
        for(Country country: countries){
            country.setDeleted(true);
            countryRepository.save(country);
        }
    }

    @Override
    public CountryDTO updateCountry(CountryDTO countryDTO, int id) {
        Country updateCountry = countryRepository.findById(id).orElseThrow(() -> new EntityNotFoundException("Country not found for id: " + id));

        updateCountry.setName(countryDTO.getName());
        Country savedCountry = countryRepository.save(updateCountry);

        return convertEntityToDto(savedCountry);
    }
    @Override
    public CountryDTO convertEntityToDto(Country country){
        CountryDTO countryDTO = new CountryDTO();

        countryDTO.setId(country.getId());
        countryDTO.setName(country.getName());

        return countryDTO;
    }

    @Override
    public Country convertDtoToEntity(CountryDTO countryDTO) {
        Country country = new Country();

        country.setId(countryDTO.getId());
        country.setName(countryDTO.getName());

        return  country;
    }
}
