package com.vegaitproject.VegaITproject.service;

import com.vegaitproject.VegaITproject.dto.ClientDTO;
import com.vegaitproject.VegaITproject.models.Client;
import org.springframework.data.domain.Page;

public interface ClientService {

    public ClientDTO saveClient(ClientDTO client);

    public Page<ClientDTO> getAllClients(int page, int size);

    public ClientDTO getClientById(int id);

    public void deleteClientById(int id);

    public void deleteAllClients();

    public Page<ClientDTO> getByNameLike(int pageNumber, int size, String keyword, boolean isLetter);

    public ClientDTO updateClient(ClientDTO clientDTO, int id);

    public ClientDTO convertEntityToDto(Client client);

    public Client convertDtoToEntity(ClientDTO clientDTO);
}
