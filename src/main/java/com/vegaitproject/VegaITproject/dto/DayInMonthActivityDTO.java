package com.vegaitproject.VegaITproject.dto;

import lombok.Data;
import java.time.LocalDate;

@Data
public class DayInMonthActivityDTO {

    private LocalDate date;
    private String day;
    private float hours;
    private String enabledOrDisabled;
}
