package com.vegaitproject.VegaITproject.dto;

import lombok.Data;

@Data
public class CalendarDTO {

    private Integer month;

    private Integer year;

    private Integer teamMemberId;
}
