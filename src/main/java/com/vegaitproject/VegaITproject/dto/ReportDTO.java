package com.vegaitproject.VegaITproject.dto;

import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import java.time.LocalDate;

@Data
public class ReportDTO {

    private Integer clientId;

    private Integer projectId;

    private Integer categoryId;

    private Integer teamMemberId;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate startDate;

    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private LocalDate endDate;

    private String sort;
}
