package com.vegaitproject.VegaITproject.dto;

import com.vegaitproject.VegaITproject.enums.MemberStatus;
import com.vegaitproject.VegaITproject.enums.Role;
import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class TeamMemberDTO {

    private int id;

    @NotNull
    @NotEmpty(message = "Please provide a name")
    @Size(min = 2, max = 32, message = "Name must be between 2 and 32 characters long")
    private String name;

    @NotNull
    @NotEmpty
    @Size(min = 2, max = 32, message = "Name must be between 2 and 32 characters long")
    private String username;

    @NotNull
    @NotEmpty
    private String email;

    @NotNull
    private Float dailyHours;

    @NotNull
    private MemberStatus status;

    @NotNull
    private Role role;
}
