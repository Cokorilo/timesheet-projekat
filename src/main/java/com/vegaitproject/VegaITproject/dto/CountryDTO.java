package com.vegaitproject.VegaITproject.dto;

import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class CountryDTO {

    private int id;

    @NotEmpty
    @NotNull
    @Size(min = 2, max = 32, message = "Name must be between 2 and 32 characters long")
    private String name;
}
