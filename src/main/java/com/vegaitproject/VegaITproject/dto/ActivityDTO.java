package com.vegaitproject.VegaITproject.dto;

import lombok.Data;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Data
public class ActivityDTO {

    private int id;

    @NotNull
    private int clientId;

    private String clientName;

    @NotNull
    private int projectId;

    private String projectName;

    @NotNull
    private int categoryId;

    private String categoryName;

    @NotNull
    private int teamMemberId;

    private String teamMemberName;

    @NotNull
    private LocalDate date;

    @NotNull
    private float time;

    private String description;
}
