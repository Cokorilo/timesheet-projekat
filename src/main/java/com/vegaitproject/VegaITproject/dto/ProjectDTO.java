package com.vegaitproject.VegaITproject.dto;

import com.vegaitproject.VegaITproject.enums.ProjectStatus;
import lombok.Data;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

@Data
public class ProjectDTO {

    private int id;
    @NotNull
    @NotEmpty
    @Size(min = 2, max = 32, message = "Name must be between 2 and 32 characters long")
    private String name;

    @NotNull
    private int customerId;

    @NotNull
    private int leadId;

    @NotNull
    private ProjectStatus status;

    private boolean archive;
}
