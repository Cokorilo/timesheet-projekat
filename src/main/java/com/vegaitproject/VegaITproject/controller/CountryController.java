package com.vegaitproject.VegaITproject.controller;

import com.vegaitproject.VegaITproject.dto.CountryDTO;
import com.vegaitproject.VegaITproject.service.CountryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/country")
public class CountryController {

    @Autowired
    private CountryService countryService;

    @GetMapping("/getAll")
    private List<CountryDTO> getAllCountries(){
        return countryService.getAllCountries();
    }


    @GetMapping("/get")
    private CountryDTO getCountryById(@RequestParam int id){
        return countryService.getCountryById(id);
    }


    @PostMapping("/add")
    private CountryDTO add(@Valid @RequestBody CountryDTO countryDTO){
        return countryService.saveCountry(countryDTO);
    }


    @DeleteMapping("/deleteAll")
    private void delete(){
        countryService.deleteAllCountries();
    }


    @DeleteMapping("/delete")
    private void deleteById(@RequestParam int id){
        countryService.deleteCountryById(id);
    }


    @PutMapping("/update")
    private CountryDTO update(@Valid @RequestBody CountryDTO countryDTO, @RequestParam int id){
        return countryService.updateCountry(countryDTO, id);
    }
}
