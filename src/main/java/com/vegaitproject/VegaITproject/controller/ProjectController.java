package com.vegaitproject.VegaITproject.controller;

import com.vegaitproject.VegaITproject.dto.ProjectDTO;
import com.vegaitproject.VegaITproject.service.ProjectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/project")
public class ProjectController {

    @Autowired
    private ProjectService projectService;

    @GetMapping("/getAll")
    private Page<ProjectDTO> getAllProjects(@RequestParam(name = "page", defaultValue = "0") int page,
                                            @RequestParam(name = "size", defaultValue = "5") int size){
        return projectService.getAllProjects(page, size);
    }


    @GetMapping("/get")
    private ProjectDTO getProjectById(@RequestParam int id){
        return projectService.getProjectById(id);
    }


    @GetMapping("/name")
    private Page<ProjectDTO> getProjectByName(@RequestParam(name = "page", defaultValue = "0") int page,
                                              @RequestParam(name = "size", defaultValue = "5") int size, @RequestParam(name = "keyword") String keyword,
                                              @RequestParam(name = "isLetter") boolean isLetter){
        return projectService.getByNameLike(page, size, keyword, isLetter);
    }


    @PostMapping("/add")
    private ProjectDTO add(@Valid @RequestBody ProjectDTO projectDTO){
        return projectService.saveProject(projectDTO);
    }


    @DeleteMapping("/deleteAll")
    private void delete(){
        projectService.deleteAllProjects();
    }


    @DeleteMapping("/delete")
    private void deleteById(@RequestParam int id){
        projectService.deleteProjectById(id);
    }


    @PutMapping("/update")
    private ProjectDTO update(@Valid @RequestBody ProjectDTO projectDTO, @RequestParam int id){
        return projectService.updateProject(projectDTO, id);
    }
}
