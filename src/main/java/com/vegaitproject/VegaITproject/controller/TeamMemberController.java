package com.vegaitproject.VegaITproject.controller;

import com.vegaitproject.VegaITproject.dto.TeamMemberDTO;
import com.vegaitproject.VegaITproject.service.TeamMemberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;

@RestController
@RequestMapping("/team-member")
public class TeamMemberController {

    @Autowired
    private TeamMemberService teamMemberService;

    @GetMapping("/getAll")
    private Page<TeamMemberDTO> getAllTeamMembers(@RequestParam(name = "page", defaultValue = "0") int page,
                                                  @RequestParam(name = "size", defaultValue = "5") int size){
        return teamMemberService.getAllTeamMembers(page, size);
    }


    @GetMapping("/get")
    private TeamMemberDTO getTeamMemberById(@RequestParam int id){
        return teamMemberService.getTeamMemberById(id);
    }


    @PostMapping("/add")
    private TeamMemberDTO add(@Valid @RequestBody TeamMemberDTO teamMemberDTO){
        return teamMemberService.saveTeamMember(teamMemberDTO);
    }


    @DeleteMapping("/deleteAll")
    private void delete(){
        teamMemberService.deleteAllTeamMembers();
    }


    @DeleteMapping("/delete")
    private void deleteById(@RequestParam int id){
        teamMemberService.deleteTeamMemberById(id);
    }


    @PutMapping("/update")
    private TeamMemberDTO update(@Valid @RequestBody TeamMemberDTO teamMemberDTO, @RequestParam int id){
        return teamMemberService.updateTeamMember(teamMemberDTO, id);
    }
}
