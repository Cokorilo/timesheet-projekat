package com.vegaitproject.VegaITproject.controller;

import com.vegaitproject.VegaITproject.dto.CategoryDTO;
import com.vegaitproject.VegaITproject.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/category")
public class CategoryController {

    @Autowired
    private CategoryService categoryService;

    @GetMapping("/getAll")
    private List<CategoryDTO> getAllCategories(){
        return categoryService.getAllCategories();
    }


    @GetMapping("get")
    private CategoryDTO getCategoryById(@RequestParam int id){
        return categoryService.getCategoryById(id);
    }


    @PostMapping("/add")
    private CategoryDTO add(@Valid @RequestBody CategoryDTO categoryDTO){
        return categoryService.saveCategory(categoryDTO);
    }


    @DeleteMapping("/deleteAll")
    private void delete(){
        categoryService.deleteAllCategories();
    }


    @DeleteMapping("/delete")
    private void deleteById(@RequestParam int id){
        categoryService.deleteCategoryById(id);
    }


    @PutMapping("/update")
    private CategoryDTO update(@Valid @RequestBody CategoryDTO categoryDTO, @RequestParam int id){
        return categoryService.updateCategory(categoryDTO, id);
    }
}
