package com.vegaitproject.VegaITproject.controller;

import com.vegaitproject.VegaITproject.dto.ActivityDTO;
import com.vegaitproject.VegaITproject.dto.CalendarDTO;
import com.vegaitproject.VegaITproject.dto.DayInMonthActivityDTO;
import com.vegaitproject.VegaITproject.dto.ReportDTO;
import com.vegaitproject.VegaITproject.service.ActivityService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/activity")
public class ActivityController {

    @Autowired
    private ActivityService activityService;


    @GetMapping("/getAll")
    private List<ActivityDTO> getAllActivities(){
        return activityService.getAllActivities();
    }


    @GetMapping("/get")
    private ActivityDTO getActivityById(@RequestParam int id){
        return activityService.getActivityById(id);
    }


   @GetMapping("/reports")
    private Page<ActivityDTO> findActivityInReports(@RequestBody ReportDTO report,
                                                    @RequestParam(name = "page", defaultValue = "0") int page,
                                                    @RequestParam(name = "size", defaultValue = "5") int size){
        return activityService.getActivityInReports(report, page, size);
    }


    @GetMapping("/calendar")
    private List<DayInMonthActivityDTO> calendar(@RequestBody CalendarDTO calendarDTO){
        return activityService.returnCalendar(calendarDTO);
    }
    @GetMapping("/byDate")
    private List<ActivityDTO> getActivityByDate(@RequestParam @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate date, @RequestParam int id){
        return activityService.returnActivityByDate(date, id);
    }

    @PostMapping("/add")
    private ActivityDTO add(@Valid @RequestBody ActivityDTO activityDTO){
        return activityService.saveActivity(activityDTO);
    }


    @DeleteMapping("/deleteAll")
    private void delete(){
        activityService.deleteAllActivities();
    }


    @DeleteMapping("/delete")
    private void deleteById(@RequestParam int id){
        activityService.deleteActivityById(id);
    }


    @PutMapping("/update")
    private ActivityDTO update(@Valid @RequestBody ActivityDTO activityDTO, @RequestParam int id){
        return activityService.updateActivity(activityDTO, id);
    }
}
