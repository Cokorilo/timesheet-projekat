package com.vegaitproject.VegaITproject.controller;

import com.vegaitproject.VegaITproject.dto.ClientDTO;
import com.vegaitproject.VegaITproject.service.ClientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/client")
public class ClientController {

    @Autowired
    private ClientService clientService;

    @GetMapping("/getAll")
    private Page<ClientDTO> getAllClients(@RequestParam(name = "page", defaultValue = "0") int page,
                                          @RequestParam(name = "size", defaultValue = "5") int size){
        return clientService.getAllClients(page, size);
    }


    @GetMapping("get")
    private ClientDTO getClientById(@RequestParam int id){
        return clientService.getClientById(id);
    }


    @GetMapping("/name")
    private Page<ClientDTO> getClientByName(@RequestParam(name = "page", defaultValue = "0") int page,
                                            @RequestParam(name = "size", defaultValue = "5") int size, @RequestParam(name = "keyword") String keyword,
                                            @RequestParam(name = "isLetter") boolean isLetter){
        return clientService.getByNameLike(page, size, keyword, isLetter);
    }


    @PostMapping("/add")
    private ClientDTO add(@Valid @RequestBody ClientDTO clientDTO){
        return clientService.saveClient(clientDTO);
    }


    @DeleteMapping("/deleteAll")
    private void delete(){
        clientService.deleteAllClients();
    }


    @DeleteMapping("/delete")
    private void deleteById(@RequestParam int id){
        clientService.deleteClientById(id);
    }


    @PutMapping("/update")
    private ClientDTO update(@Valid @RequestBody ClientDTO clientDTO, @RequestParam int id){
        return clientService.updateClient(clientDTO, id);
    }
}
