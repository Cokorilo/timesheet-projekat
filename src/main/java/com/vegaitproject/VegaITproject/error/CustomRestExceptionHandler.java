package com.vegaitproject.VegaITproject.error;

import org.hibernate.QueryException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import java.time.DateTimeException;
import java.util.ArrayList;
import java.util.List;

@ControllerAdvice
public class CustomRestExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex) {
        final List<String> errors = new ArrayList<String>();
        for (final FieldError error : ex.getBindingResult().getFieldErrors()) {
            errors.add(error.getField() + ": " + error.getDefaultMessage());
        }
        for (final ObjectError error : ex.getBindingResult().getGlobalErrors()) {
            errors.add(error.getObjectName() + ": " + error.getDefaultMessage());
        }

        return new ResponseEntity<>(errors, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(EntityNotFoundException.class)
    protected ResponseEntity<Object> NotFound(EntityNotFoundException ex) {

        return new ResponseEntity<>("Could not find entity with provided id", HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(MethodArgumentTypeMismatchException .class)
    protected ResponseEntity<String> handleTypeMismatch(MethodArgumentTypeMismatchException ex){

        return new ResponseEntity<>("Could not find entity, failed to convert value to required type", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(MissingPathVariableException.class)
    protected ResponseEntity<String> missingPathVariable(MissingPathVariableException ex){

        return new ResponseEntity<>("Required URI template variable method parameter type is not present", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(DateTimeException.class)
    protected ResponseEntity<String> dateTimeException(DateTimeException ex){

        return new ResponseEntity<>("Invalid value for date", HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(QueryException.class)
    protected ResponseEntity<String> queryException(QueryException ex){

        return new ResponseEntity<>("Could not resolve property", HttpStatus.BAD_REQUEST);
    }
}
